from random import *


class MRZGenerator:

    def __init__(self, surname, surname2, name, sexe, date, dep):
        self.surname = surname.replace("-", "<").replace(" ", "<").upper()
        self.surname2 = surname2.replace("-", "<").replace(" ", "<").upper()
        self.name = name.replace("-", "<").upper()
        self.sexe = sexe
        self.date = date
        self.dep = dep

    def formatName(self):
        return self.name + "".join(["<" for i in range(0, 25 - len(self.name))]) if len(self.name) <= 25 else self.name[:25]

    def formatSurname(self):
        return self.surname[:13] if len(self.surname) >= 13 else \
            self.surname + "".join(["<" for i in range(0, 14 - len(self.surname))]) if len(self.surname2) <= 1 else \
                self.surname + "<<" + self.surname2 + "".join(["<" for i in range(0, 14 - (len(self.surname2) + len(self.surname) + 2))]) if (len(self.surname)+len(self.surname2)+2) <= 13 else \
                    self.surname + "<<" + self.surname2[:len(self.surname+"<<")]

    def key(self, code):
        result = 0
        fact = (7, 3, 1)

        for (position, car) in enumerate(code):
            if car == "<":
                valeur = 0
            elif "0" <= car <= "9":
                valeur = int(car)
            elif "A" <= car <= "Z":
                valeur = ord(car) - 55
            else:
                break
            result += valeur * fact[position % 3]

        return str(result % 10)

    def mrz(self):
        print( ("".join(self.date[2-4:]+self.date[3:5]) + self.dep + "0" + str(randrange(10000, 99999))) )
        print( "".join(self.date[2-4:]+self.date[3:5]+self.date[:2]) )

        key1 = self.key(("".join(self.date[2-4:]+self.date[3:5]) + self.dep + "0" + str(randrange(10000, 99999))))
        key2 = self.key("".join(self.date[2-4:]+self.date[3:5]+self.date[:2]))
        print(key2)

        l1 = "ID" + "FRA" + self.formatName() + str(self.dep + str(randrange(1000, 9999)))
        l2 = "".join(self.date[2-4:]+self.date[3:5]) + "0" + self.dep + str(randrange(10000, 99999)) + \
               key1 + str(self.formatSurname()) + "".join(self.date[2-4:]+self.date[3:5]+self.date[:2]) + key2 + self.sexe
        key3 = self.key(l1+l2)
        return str(l1 + "\n" + l2 + key3)


if __name__ == '__main__':

    print("\nMRZ Generator")
    mrz = MRZGenerator("Mariette", "Rita", "Fournier", "F", "04/10/1991", "91")
    print(mrz.mrz())
